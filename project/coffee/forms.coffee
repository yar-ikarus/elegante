$(document).ready ->
    $("#any-form .btn").click ->
        sendForm("#any-form")

    $("#main-request-form .btn").click ->
        sendForm("#main-request-form")

    $("#modal-callback-form .btn").click ->
        sendForm("#modal-callback-form")

    $("#index-phone-form .btn").click ->
        sendForm("#index-phone-form")

    $("#main-callback-form .btn").click ->
        sendForm("#main-callback-form")

sendForm = (form) -> 
    form = $(form)
    form.validate()
    if form.valid()
        form.find('.btn').attr('disabled', 'disabled')
        form.slideUp().parent().append('<h4 class="text-center">В ближайшее время наш менеджер свяжется с вами.</h4>')
        return $.post("/forms.php", form.serialize()).done((data) -> {})