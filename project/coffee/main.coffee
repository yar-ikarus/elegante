$(document).ready ->

	$(".open-dropdown-list").click ->
		if $(this).hasClass "open"
			$(this).removeClass "open"
			$(".dropdown-list").slideUp(333)
			$(this).find("span").html("Открыть расширенный фильтр")
			$(this).find(".icons-close-dropdown").hide()
			$(this).find(".icons-open-dropdown").show()
		else 
			$(this).find(".icons-close-dropdown").show()
			$(this).find(".icons-open-dropdown").hide()
			$(this).addClass "open"
			$(".dropdown-list").slideDown(333)
			$(this).find("span").html("Закрыть расширенный фильтр")

	$(".dropdown-list .list li").click ->
		$(this).find(".drop").slideToggle(333)

	$(".page-order input").on 'change', (e) ->
		#расчет стоимости в ордере
		final = $("#final-price")
		finalPrice = final.attr("data-price")
		unit = $(this).closest("tr").find(".unit-price span").attr("data-price")
		group = $(this).closest("tr").find(".group-price span")
		quantity = $(this).val()

		groupPrice = quantity * unit
		group.attr("data-price", groupPrice)
		group.html(separator(groupPrice)+" руб.")

		allPrice = []
		#узнаем финальную стоимость всех товаров
		$(".group-price span").each ->
			currentPrice = $(this).attr("data-price")
			allPrice.push currentPrice
		i = 0
		allPriceFinal = 0
		while i < allPrice.length
			allPriceFinal = parseInt(allPriceFinal) + parseInt(allPrice[i])
			i++
		final.html separator(allPriceFinal) + " руб."

	$(".product-slider-small .img").click ->
		open = $(this).attr "data-open"
		mainImg = $(".product .level-0 img")
		mainImg.attr
			"src": open
			"data-large": open

	$(".product-description .tabs div").click ->
		open = $(this).attr "data-open"
		$(".hidden-places").slideUp 333
		$(".section-"+open).slideDown 333
		$(".product-description .tabs div").removeClass "active"
		$(this).addClass "active"


	$(".page-catalog .sort li").click ->
		$(this).closest("ul").find("li").removeClass("active")
		$(this).addClass "active"


	$(".add-to-cart-success .close").click ->
		$(this).closest(".add-to-cart-success").slideUp(108)

	$('#search input[name=\'search\']').parent().find('.btn').on 'click', ->
		url = $('base').attr('href') + 'index.php?route=product/search'
		value = $('header input[name=\'search\']').val()
		if value
			url += '&search=' + encodeURIComponent(value)
		location = url

	$('#search input[name=\'search\']').on 'keydown', (e) ->
		if e.keyCode == 13
			$('header input[name=\'search\']').parent().find('.btn').trigger 'click'

	$(".page-card .tabs span").click ->
		open = $(this).attr("data-open")
		$(".page-card .hidden-places").slideUp(333)
		$(".page-card .part-"+open).slideDown(333)
		$(this).closest(".tabs").find("span").removeClass("active")
		$(this).addClass("active")

	$(".quantity span").click ->
		object = $(this).closest(".quantity").find("input")
		curval = parseInt object.val()
		if $(this).hasClass("plus")
			curval += 1
		else
			curval -= 1
		if curval < 1
			curval = 0
		object.val curval
		countProducts(curval)


	$(".show-cut").click ->
		if $(this).hasClass "active"
			$(this).removeClass "active"
			$(this).closest(".container").find(".cut").slideUp(333)
			$(this).html "Подробнее"
		else
			$(this).addClass "active"
			$(this).closest(".container").find(".cut").slideDown(333)
			$(this).html "Скрыть"

	month = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
	days = ['Вс','Пн','Вт','Ср','Чт','Пт','Сб']
	
	$(".datepicker").datepicker(
		inline: yes
		monthNames: month
		dayNamesMin: days
		firstDay: 1
	)
	
	$(".submit-this").click ->
		$(this).closest("form").submit()

	$('input').iCheck
		checkboxClass: 'icheckbox_minimal-red'
		radioClass: 'iradio_minimal-red'
		increaseArea: '20%'

	$(".slider").owlCarousel
		navigation: yes
		slideSpeed: 444
		paginationSpeed: 333
		singleItem: yes
		touchDrag: yes
		autoPlay: 7777
		navigationText: no

	$(".slider-4-pagination").owlCarousel
		navigation: yes
		slideSpeed: 444
		paginationSpeed: 333
		touchDrag: yes
		autoPlay: 7777
		items: 4
		navigationText: no
		pagination: yes

	$(".product-slider-small").owlCarousel
		navigation: no
		slideSpeed: 444
		paginationSpeed: 333
		touchDrag: yes
		singleItem: no
		autoPlay: 7777
		items: 4
		navigationText: no
		pagination: no
	
	resizeModalWindows()
	resizeSandwich()
	resizeCells()

	$(".get-callback").click ->
		openModalWindow("#callback-form-place")
		return false
	
	$(".modal-window .icons-close, .darkness, .sandwich-menue .title").click ->
		closeModalWindows()
		closeSandwich()

	# /menue/
	$(".menue-list .title").click ->		
		$(this).closest(".section").find(".row").slideToggle(108)
		$(this).toggleClass("active")

	# adaptive menue
	$(".open-adaptive-menue").click ->
		openSandwich()		

	$(".open-hidden-buttons").click ->
		closeAdaptiveMenue()
		$(".hidden-buttons-menue").show()
		$(".darkness").show()

$(window).resize ->
	resizeModalWindows()
	resizeSandwich()
	resizeCells()

openModalWindow = (xx) ->
	closeAdaptiveMenue()
	xx = $(xx)
	$(".modal-window").hide()
	xx.show()
	xx.css
		"position": "absolute"
	$(".wrapper").css
		"position": "fixed"
	$(".darkness").show()

closeModalWindows = ->
	xx = $(".modal-window")
	xx.hide()
	xx.css
		"position": "absolute"
	$(".wrapper").css
		"position": "relative"
	$(".darkness").hide()
	closeAdaptiveMenue()

closeAdaptiveMenue = ->
	$(".adaptive-menue").hide()

resizeModalWindows = ->
	wHeight = $(window).height() / 10
	$(".modal-window .place").css
		"marginTop": wHeight

resizeSandwich = ->
	wWidth = $(window).width()
	wHeight = $(window).height()
	denominator = 3

	if wWidth < 800 
		denominator = 2
	if wWidth < 700
		denominator = 1.5	
	if wWidth < 600
		denominator = 1.25

	$(".sandwich-menue").width(wWidth / denominator)
	$(".sandwich-menue").css
		# "min-height":wHeight

openSandwich = ->
	$(".darkness").show()
	$(".sandwich-menue").css
		"left": 0
		"position": "absolute"
	$(".wrapper").css
		"position": "fixed"
closeSandwich = ->
	$(".sandwich-menue").css
		"left": -1000
	$(".wrapper").css
		"position": "relative"

countProducts = (count) ->
	cart_price = $("#product-price").attr('first-price')
	price = cart_price*Number(count)
	total = price.toFixed(2)
	return $("#product-price").html(total + "р")

resizeCells = ->
	#функция которая делает одинаковой высоту во всех блоках .cell
	object = $(".cell-need-resize .cell")
	blocksArray = []
	object.each ->
		blockHeight = $(this).height()
		blocksArray.push blockHeight
	#а теперь узнаем самое большое значение в массиве
	blocksMax = Math.max.apply null, blocksArray
	#это и есть высота которая будет задана всем блокам
	object.css 
		"height": blocksMax

separator = (n) ->
	(n + '').split('').reverse().join('').replace(/(\d{3})/g, '$1 ').split('').reverse().join('').replace /^ /, ''