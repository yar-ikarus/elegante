(function() {
  var closeAdaptiveMenue, closeModalWindows, closeSandwich, countProducts, openModalWindow, openSandwich, resizeCells, resizeModalWindows, resizeSandwich, separator;

  $(document).ready(function() {
    var days, month;
    $(".open-dropdown-list").click(function() {
      if ($(this).hasClass("open")) {
        $(this).removeClass("open");
        $(".dropdown-list").slideUp(333);
        $(this).find("span").html("Открыть расширенный фильтр");
        $(this).find(".icons-close-dropdown").hide();
        return $(this).find(".icons-open-dropdown").show();
      } else {
        $(this).find(".icons-close-dropdown").show();
        $(this).find(".icons-open-dropdown").hide();
        $(this).addClass("open");
        $(".dropdown-list").slideDown(333);
        return $(this).find("span").html("Закрыть расширенный фильтр");
      }
    });
    $(".dropdown-list .list li").click(function() {
      return $(this).find(".drop").slideToggle(333);
    });
    $(".page-order input").on('change', function(e) {
      var allPrice, allPriceFinal, final, finalPrice, group, groupPrice, i, quantity, unit;
      final = $("#final-price");
      finalPrice = final.attr("data-price");
      unit = $(this).closest("tr").find(".unit-price span").attr("data-price");
      group = $(this).closest("tr").find(".group-price span");
      quantity = $(this).val();
      groupPrice = quantity * unit;
      group.attr("data-price", groupPrice);
      group.html(separator(groupPrice) + " руб.");
      allPrice = [];
      $(".group-price span").each(function() {
        var currentPrice;
        currentPrice = $(this).attr("data-price");
        return allPrice.push(currentPrice);
      });
      i = 0;
      allPriceFinal = 0;
      while (i < allPrice.length) {
        allPriceFinal = parseInt(allPriceFinal) + parseInt(allPrice[i]);
        i++;
      }
      return final.html(separator(allPriceFinal) + " руб.");
    });
    $(".product-slider-small .img").click(function() {
      var mainImg, open;
      open = $(this).attr("data-open");
      mainImg = $(".product .level-0 img");
      return mainImg.attr({
        "src": open,
        "data-large": open
      });
    });
    $(".product-description .tabs div").click(function() {
      var open;
      open = $(this).attr("data-open");
      $(".hidden-places").slideUp(333);
      $(".section-" + open).slideDown(333);
      $(".product-description .tabs div").removeClass("active");
      return $(this).addClass("active");
    });
    $(".page-catalog .sort li").click(function() {
      $(this).closest("ul").find("li").removeClass("active");
      return $(this).addClass("active");
    });
    $(".add-to-cart-success .close").click(function() {
      return $(this).closest(".add-to-cart-success").slideUp(108);
    });
    $('#search input[name=\'search\']').parent().find('.btn').on('click', function() {
      var location, url, value;
      url = $('base').attr('href') + 'index.php?route=product/search';
      value = $('header input[name=\'search\']').val();
      if (value) {
        url += '&search=' + encodeURIComponent(value);
      }
      return location = url;
    });
    $('#search input[name=\'search\']').on('keydown', function(e) {
      if (e.keyCode === 13) {
        return $('header input[name=\'search\']').parent().find('.btn').trigger('click');
      }
    });
    $(".page-card .tabs span").click(function() {
      var open;
      open = $(this).attr("data-open");
      $(".page-card .hidden-places").slideUp(333);
      $(".page-card .part-" + open).slideDown(333);
      $(this).closest(".tabs").find("span").removeClass("active");
      return $(this).addClass("active");
    });
    $(".quantity span").click(function() {
      var curval, object;
      object = $(this).closest(".quantity").find("input");
      curval = parseInt(object.val());
      if ($(this).hasClass("plus")) {
        curval += 1;
      } else {
        curval -= 1;
      }
      if (curval < 1) {
        curval = 0;
      }
      object.val(curval);
      return countProducts(curval);
    });
    $(".show-cut").click(function() {
      if ($(this).hasClass("active")) {
        $(this).removeClass("active");
        $(this).closest(".container").find(".cut").slideUp(333);
        return $(this).html("Подробнее");
      } else {
        $(this).addClass("active");
        $(this).closest(".container").find(".cut").slideDown(333);
        return $(this).html("Скрыть");
      }
    });
    month = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
    days = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
    $(".datepicker").datepicker({
      inline: true,
      monthNames: month,
      dayNamesMin: days,
      firstDay: 1
    });
    $(".submit-this").click(function() {
      return $(this).closest("form").submit();
    });
    $('input').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red',
      increaseArea: '20%'
    });
    $(".slider").owlCarousel({
      navigation: true,
      slideSpeed: 444,
      paginationSpeed: 333,
      singleItem: true,
      touchDrag: true,
      autoPlay: 7777,
      navigationText: false
    });
    $(".slider-4-pagination").owlCarousel({
      navigation: true,
      slideSpeed: 444,
      paginationSpeed: 333,
      touchDrag: true,
      autoPlay: 7777,
      items: 4,
      navigationText: false,
      pagination: true
    });
    $(".product-slider-small").owlCarousel({
      navigation: false,
      slideSpeed: 444,
      paginationSpeed: 333,
      touchDrag: true,
      singleItem: false,
      autoPlay: 7777,
      items: 4,
      navigationText: false,
      pagination: false
    });
    resizeModalWindows();
    resizeSandwich();
    resizeCells();
    $(".get-callback").click(function() {
      openModalWindow("#callback-form-place");
      return false;
    });
    $(".modal-window .icons-close, .darkness, .sandwich-menue .title").click(function() {
      closeModalWindows();
      return closeSandwich();
    });
    $(".menue-list .title").click(function() {
      $(this).closest(".section").find(".row").slideToggle(108);
      return $(this).toggleClass("active");
    });
    $(".open-adaptive-menue").click(function() {
      return openSandwich();
    });
    return $(".open-hidden-buttons").click(function() {
      closeAdaptiveMenue();
      $(".hidden-buttons-menue").show();
      return $(".darkness").show();
    });
  });

  $(window).resize(function() {
    resizeModalWindows();
    resizeSandwich();
    return resizeCells();
  });

  openModalWindow = function(xx) {
    closeAdaptiveMenue();
    xx = $(xx);
    $(".modal-window").hide();
    xx.show();
    xx.css({
      "position": "absolute"
    });
    $(".wrapper").css({
      "position": "fixed"
    });
    return $(".darkness").show();
  };

  closeModalWindows = function() {
    var xx;
    xx = $(".modal-window");
    xx.hide();
    xx.css({
      "position": "absolute"
    });
    $(".wrapper").css({
      "position": "relative"
    });
    $(".darkness").hide();
    return closeAdaptiveMenue();
  };

  closeAdaptiveMenue = function() {
    return $(".adaptive-menue").hide();
  };

  resizeModalWindows = function() {
    var wHeight;
    wHeight = $(window).height() / 10;
    return $(".modal-window .place").css({
      "marginTop": wHeight
    });
  };

  resizeSandwich = function() {
    var denominator, wHeight, wWidth;
    wWidth = $(window).width();
    wHeight = $(window).height();
    denominator = 3;
    if (wWidth < 800) {
      denominator = 2;
    }
    if (wWidth < 700) {
      denominator = 1.5;
    }
    if (wWidth < 600) {
      denominator = 1.25;
    }
    $(".sandwich-menue").width(wWidth / denominator);
    return $(".sandwich-menue").css;
  };

  openSandwich = function() {
    $(".darkness").show();
    $(".sandwich-menue").css({
      "left": 0,
      "position": "absolute"
    });
    return $(".wrapper").css({
      "position": "fixed"
    });
  };

  closeSandwich = function() {
    $(".sandwich-menue").css({
      "left": -1000
    });
    return $(".wrapper").css({
      "position": "relative"
    });
  };

  countProducts = function(count) {
    var cart_price, price, total;
    cart_price = $("#product-price").attr('first-price');
    price = cart_price * Number(count);
    total = price.toFixed(2);
    return $("#product-price").html(total + "р");
  };

  resizeCells = function() {
    var blocksArray, blocksMax, object;
    object = $(".cell-need-resize .cell");
    blocksArray = [];
    object.each(function() {
      var blockHeight;
      blockHeight = $(this).height();
      return blocksArray.push(blockHeight);
    });
    blocksMax = Math.max.apply(null, blocksArray);
    return object.css({
      "height": blocksMax
    });
  };

  separator = function(n) {
    return (n + '').split('').reverse().join('').replace(/(\d{3})/g, '$1 ').split('').reverse().join('').replace(/^ /, '');
  };

}).call(this);

