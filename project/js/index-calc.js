(function() {
  var calculation, steps;

  $(document).ready(function() {
    $("#svg-map .region").click(function() {
      $("#svg-map .region").attr("class", "region");
      return $(this).attr("class", "active region");
    });
    $(".calculator-module input").on("ifToggled", function(event) {
      return calculation();
    });
    $(".calculator-module .options .step-1 input").on("ifToggled", function(event) {
      return steps($(this));
    });
    $(".calculator-module .options .step-2 input").on("ifToggled", function(event) {
      return steps($(this));
    });
    return $("input[name='containers-count']").keydown(function() {
      steps($(this));
      return console.log("TEST");
    });
  });

  steps = function(object) {
    var next, step;
    step = parseInt(object.closest(".row").attr("data-step"));
    next = step + 1;
    $(".calculator-module .steps .step-" + step).addClass("valid");
    return $(".calculator-module .options .step-" + next).removeClass("disabled");
  };

  calculation = function() {
    var data, object, priceType;
    data = [];
    priceType = 0;
    object = $(".calculator-module");
    return $('.calculator-module input').each(function() {
      if (this.checked) {
        return priceType = $(this).attr("data-price-type");
      }
    });
  };

}).call(this);

