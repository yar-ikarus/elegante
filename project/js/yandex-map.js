(function() {

  ymaps.ready(init);
  var myMap, salon1, salon2, salon3;
  var balun = {
    iconLayout: 'default#image',
      iconImageHref: '../images/icons/big-balun.png',
      iconImageSize: [48, 73],
      iconImageOffset: [-3, -42]
  };

  function init(){     
      myMap = new ymaps.Map("YMapsID", {
          center: [55.74774700674332,37.61727183146526],
          zoom: 10
      });
      var salon1 = new ymaps.Placemark([55.76631327714641,37.69665999999997], {
        balloonContent: 'Москва, Госпитальная площадь  ул., д. 16, (ВАО) станция метро Авиамоторная'
      }, balun);
      salon2 = new ymaps.Placemark([55.695981277374955,37.61984549999999], { 
        content: '', 
        balloonContent: 'Москва, Варшавское шоссе, д. 14, (ЮАО) станция метро Нагорная'
      }, balun);
      salon3 = new ymaps.Placemark([55.861069776750966,37.5233215], { 
        content: '', 
        balloonContent: 'Москва, ул Зеленоградская, д. 3, (САО) станция метро Водный стадион'
      }, balun);
      myMap.geoObjects.add(salon1);
      myMap.geoObjects.add(salon2);
      myMap.geoObjects.add(salon3);
  }

}).call(this);