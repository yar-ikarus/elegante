(function() {
  var sendForm;

  $(document).ready(function() {
    $("#any-form .btn").click(function() {
      return sendForm("#any-form");
    });
    $("#main-request-form .btn").click(function() {
      return sendForm("#main-request-form");
    });
    $("#modal-callback-form .btn").click(function() {
      return sendForm("#modal-callback-form");
    });
    $("#index-phone-form .btn").click(function() {
      return sendForm("#index-phone-form");
    });
    return $("#main-callback-form .btn").click(function() {
      return sendForm("#main-callback-form");
    });
  });

  sendForm = function(form) {
    form = $(form);
    form.validate();
    if (form.valid()) {
      form.find('.btn').attr('disabled', 'disabled');
      form.slideUp().parent().append('<h4 class="text-center">В ближайшее время наш менеджер свяжется с вами.</h4>');
      return $.post("/forms.php", form.serialize()).done(function(data) {
        return {};
      });
    }
  };

}).call(this);

