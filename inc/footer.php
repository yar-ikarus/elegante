<div class="footer">
	<div class="level-0">
		<div class="container">
			<div class="row logo">
				<a href="/"><i class="icons-header-logo"></i></a>
			</div>
			<!-- / -->
			<div class="row address">
				<div><a href="">г. Москва, ул.Мира, д.3</a></div>
				<div><a href="">станция метро “Горьковская”</a></div>
			</div>
			<!-- / -->
			<div class="row address">
				<div><a href="">г. Москва, ул.Мира, д.3</a></div>
				<div><a href="">станция метро “Горьковская”</a></div>
			</div>
			<div class="row phone-place">
				<div><a href="tel:+79626596565">+7 (962) 659 65 65</a></div>
				<div>ежедневно c 8:00 до 21:00</div>
			</div>
			<div class="row callback">
				<div class="btn">Обратный звонок</div>
			</div>
			<div class="row cart">
				<div class="btn"><span>Корзина</span></div>
			</div>
		</div>
	</div>
	<!-- / -->
	<div class="level-1">
		<div class="container">
			<div class="row">
				<div class="title">
					Информация
				</div>
				<ul>
					<li><a href="/">О компании</a></li>
					<li><a href="/">Доставка</a></li>
					<li><a href="/">Оплата</a></li>
					<li><a href="/">Установка и подключение</a></li>
					<li><a href="/">Контакты</a></li>
				</ul>
			</div>
			<!-- / -->
			<div class="row">
				<div class="title">
					Категории товаров
				</div>
				<ul>
					<li><a href="/">Мебель для ванной</a></li>
					<li><a href="/">Смесители</a></li>
					<li><a href="/">Ванны</a></li>
					<li><a href="/">Унитазы</a></li>
					<li><a href="/">Раковины</a></li>
					<li><a href="/">Полотенцесушители</a></li>
				</ul>
			</div>
			<!-- / -->
			<div class="row">
				<div class="form">
					<div class="title">Оставьте телефон и мы вам перезвоним</div>
					<div><input type="text" placeholder="Телефон" class="phone" name="phone"></div>
					<div><span class="btn blue">Оставить телефон</span></div>
				</div>
			</div>
			<!-- / -->
			<div class="row">
				<div class="authorization">
					<a href=""><i class="icons-login"></i>Вход</a>
					<span>/</span>
					<a href="">Регистрация</a>
				</div>
				<div class="social">
					<a href=""><i class="icons-social-vk"></i></a>
					<a href=""><i class="icons-social-in"></i></a>
					<a href=""><i class="icons-social-fb"></i></a>
					<a href=""><i class="icons-social-google"></i></a>
					<a href=""><i class="icons-social-tw"></i></a>
				</div>
			</div>
		</div>
	</div>
	<!-- / -->
	<div class="level-2">
		<div class="container">
			<span>Elegante © Москва 2010-2015 Все права защищены.</span>
		</div>
	</div>
</div>