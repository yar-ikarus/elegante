<div class="place product-list">
	<div class="title">
		<h3>Акции и распродажа</h3>
		<a href="?" class="btn">смотреть все</a>
	</div>
	<!-- / -->
	<div class="group">
		<? for ($i=0; $i < 4; $i++) { ?>
		<div class="cell size-25">
			<div class="level-0" style="background-image: url(/project/images/other/printer.jpg);">
				<div class="status green">Акция</div>
			</div>
			<div class="level-1">
					<div>3D Принтер Picaso 3D Desiger (Пикассо)</div>
				</div>
				<div class="level-2">
					<div>
						<span>Толщина слоя:</span>
						<span>100-350 микрон</span>
					</div>
					<div>
						<span>Область печати:</span>
						<span>120х120х120</span>
					</div>
					<div>
						<span>Количество головок:</span>
						<span>1</span>
					</div>
				</div>
				<div class="level-3">
					<div class="rating">
						<div class="stars">
							<span style="width: 40%"></span>
						</div>
					</div>
					<div class="price">
						100 500 Р.
					</div>
				</div>
				<div class="level-4">
					<div><a href="?">Подробнее</a></div>
					<div>
						<span class="btn red">Купить</span>
					</div>
				</div>
		</div>	
		<div class="cell size-25">
			<div class="level-0" style="background-image: url(/project/images/other/printer.jpg);">
				<div class="status orange">Скидка</div>
			</div>
			<div class="level-1">
					<div>3D Принтер Picaso 3D Desiger (Пикассо)</div>
				</div>
				<div class="level-2">
					<div>
						<span>Толщина слоя:</span>
						<span>100-350 микрон</span>
					</div>
					<div>
						<span>Область печати:</span>
						<span>120х120х120</span>
					</div>
					<div>
						<span>Количество головок:</span>
						<span>1</span>
					</div>
				</div>
				<div class="level-3">
					<div class="rating">
						<div class="stars">
							<span style="width: 40%"></span>
						</div>
					</div>
					<div class="price">
						100 500 Р.
					</div>
				</div>
				<div class="level-4">
					<div><a href="?">Подробнее</a></div>
					<div>
						<span class="btn red">Купить</span>
					</div>
				</div>
		</div>		
		<?}?>		
	</div>
</div>