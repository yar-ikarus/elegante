<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?=$title?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,500,700,400italic&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="/project/css/style.min.css">

	<link rel="stylesheet" href="/project/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/project/css/owl.transitions.min.css">
	<script src="/project/js/lib/jquery-1.9.1.min.js"></script>
	<script src="/project/js/lib/owl.carousel.min.js"></script>
	<script src="/project/js/lib/ui/jquery-ui.min.js"></script>
	<script src="/project/js/lib/icheck.min.js"></script>
	<script src="/project/js/main.min.js"></script>
</head>
<body>
	<div class="darkness"></div>
	<div class="sandwich-menue">
		<div class="title">Меню</div>
	</div>
	<div class="wrapper">
		<div class="fixed-adaptive-block">
			<div class="sandwich"><i class="icons-sandwich"></i><span>МЕНЮ</span></div>
			<div class="buttons">
				<a href=""><i class="icons-adaprive-phone"></i></a>
				<a href=""><i class="icons-adaprive-point"></i></a>
				<a href=""><i class="icons-adaprive-cart"></i></a>
			</div>
		</div>
		<div class="adaptive-header">
			<div>
				<a href="/"><i class="icons-adaptive-logo"></i></a>
				<div>Интернет-магазин <br>сантехники</div>
			</div>
		</div>
		<div class="header">
			<div class="level-0">
				<div class="container">
					<div class="row">
						<a href="/">Интернет-магазин сантехники</a>
					</div>
					<div class="row">
						<ul>
							<li><a href="">О компании</a></li>
							<li><a href="">Доставка</a></li>
							<li><a href="">Оплата</a></li>
							<li><a href="">Установка и подключение</a></li>
							<li><a href="">Контакты</a></li>
						</ul>
					</div>
					<div class="row authorization">
						<a href=""><i class="icons-login"></i>Вход</a>
						<span>/</span>
						<a href="">Регистрация</a>
					</div>
				</div>
			</div>
			<!-- / -->
			<div class="level-1">
				<div class="container">
					<div class="row logo">
						<a href="/"><i class="icons-header-logo"></i></a>
					</div>
					<!-- / -->
					<div class="row address">
						<div><a href="">г. Москва, ул.Мира, д.3</a></div>
						<div><a href="">станция метро “Горьковская”</a></div>
					</div>
					<!-- / -->
					<div class="row address">
						<div><a href="">г. Москва, ул.Мира, д.3</a></div>
						<div><a href="">станция метро “Горьковская”</a></div>
					</div>
					<div class="row phone-place">
						<div><a href="tel:+79626596565">+7 (962) 659 65 65</a></div>
						<div>ежедневно c 8:00 до 21:00</div>
					</div>
					<div class="row callback">
						<div class="btn">Обратный звонок</div>
					</div>
					<div class="row cart">
						<div class="btn"><span>Корзина</span></div>
					</div>
				</div>
			</div>
			<!-- / -->
			<div class="level-2">
				<div class="container">
					<input type="text" placeholder="Введите название или код товара" value="" name="search">
					<div class="btn pink">Поиск по сайту</div>
				</div>
			</div>
			<!-- / -->
			<div class="level-3">
				<div class="container">
					<div class="group">
						<a href="" class="cell">
							<div><i class="icons-header-level-3-1"></i></div>
							<div>Мебель для ванной</div>
						</a>
						<a href="" class="cell">
							<div><i class="icons-header-level-3-2"></i></div>
							<div>Смесители</div>
						</a>
						<a href="" class="cell">
							<div><i class="icons-header-level-3-3"></i></div>
							<div>Ванны</div>
						</a>
						<a href="" class="cell">
							<div><i class="icons-header-level-3-4"></i></div>
							<div>Унитазы</div>
						</a>
						<a href="" class="cell">
							<div><i class="icons-header-level-3-5"></i></div>
							<div>Раковины</div>
						</a>
						<a href="" class="cell">
							<div><i class="icons-header-level-3-6"></i></div>
							<div>Полотенцесушители</div>
						</a>
					</div>
				</div>
			</div>
			<!-- / -->
		</div>
		<!-- // -->