<div class="container content">
	<? include 'inc/modules/column-left.php';?>
	<div class="column-right page-default">
		<div class="breadcrumbs">
			<ul>
				<li><a href="/">Главная</a></li>
				<li><a href="/">Каталог</a></li>
			</ul>
		</div>
		<!-- / -->
		<div class="place add-to-cart-success">
			<div><a href="/cart/"><i class="icons-daw"></i>Тест <font>добавлен</font> в корзину покупок!</a><div class="close"></div></div>
		</div>
		<!-- / -->
		<div class="place show-more-title content-place">
			<div class="title-place">
				<h1>Заголовок страницы</h1>				
			</div>
			<div class="text">
				<p>Это дефолтная страница. Сюда выводим что угодно</p>	
				<p><b>Внимание!</b> Тег I зарезервирован для спрайтов! Вместо него следует использовать тег EM</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At nostrum facere distinctio, laboriosam, quibusdam rerum soluta! Consequuntur blanditiis nesciunt molestiae natus illum consectetur nulla, iusto repellat laboriosam, quisquam alias cupiditate officia, doloremque adipisci! Consectetur itaque officiis delectus praesentium ullam, soluta rem voluptatibus porro, cupiditate sint veritatis voluptatum natus at consequuntur alias? Beatae sed temporibus doloribus veritatis ducimus, esse, quod, aperiam sint laboriosam, iusto voluptatum. Ea quibusdam cum officia hic porro perspiciatis molestiae! Placeat nam sit dolor natus nostrum nesciunt voluptas consequuntur ratione, voluptatum fuga molestias in, tenetur quidem veritatis labore ab, error doloremque pariatur possimus. Modi pariatur, dolor nulla, aliquid commodi cumque, dolore recusandae provident porro eos cupiditate.</p>			
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At nostrum facere distinctio, laboriosam, quibusdam rerum soluta! Consequuntur blanditiis nesciunt molestiae natus illum consectetur nulla, iusto repellat laboriosam, quisquam alias cupiditate officia, doloremque adipisci! Consectetur itaque officiis delectus praesentium ullam, soluta rem voluptatibus porro, cupiditate sint veritatis voluptatum natus at consequuntur alias? Beatae sed temporibus doloribus veritatis ducimus, esse, quod, aperiam sint laboriosam, iusto voluptatum. Ea quibusdam cum officia hic porro perspiciatis molestiae! Placeat nam sit dolor natus nostrum nesciunt voluptas consequuntur ratione, voluptatum fuga molestias in, tenetur quidem veritatis labore ab, error doloremque pariatur possimus. Modi pariatur, dolor nulla, aliquid commodi cumque, dolore recusandae provident porro eos cupiditate.</p>			
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At nostrum facere distinctio, laboriosam, quibusdam rerum soluta! Consequuntur blanditiis nesciunt molestiae natus illum consectetur nulla, iusto repellat laboriosam, quisquam alias cupiditate officia, doloremque adipisci! Consectetur itaque officiis delectus praesentium ullam, soluta rem voluptatibus porro, cupiditate sint veritatis voluptatum natus at consequuntur alias? Beatae sed temporibus doloribus veritatis ducimus, esse, quod, aperiam sint laboriosam, iusto voluptatum. Ea quibusdam cum officia hic porro perspiciatis molestiae! Placeat nam sit dolor natus nostrum nesciunt voluptas consequuntur ratione, voluptatum fuga molestias in, tenetur quidem veritatis labore ab, error doloremque pariatur possimus. Modi pariatur, dolor nulla, aliquid commodi cumque, dolore recusandae provident porro eos cupiditate.</p>			
				<table>
					<tr>
						<th>title</th>
						<th>title</th>
						<th>title</th>
					</tr>
					<?for ($i=0; $i < 14; $i++) { ?>
					<tr>
						<td>item</td>
						<td>item</td>
						<td>item</td>
					</tr>
					<?}?>
				</table>
				<ul>
					<li>ul item 1</li>
					<li>ul item 2</li>
					<li>ul item 3</li>
					<li>ul item 4</li>
					<li>ul item 5</li>
					<li>ul item 6</li>
					<li>ul item 7</li>
					<li>ul item 8</li>
					<li>ul item 9</li>
					<li>ul item 10</li>
				</ul>
				<ol>
					<li>ol item 1</li>
					<li>ol item 2</li>
					<li>ol item 3</li>
					<li>ol item 4</li>
					<li>ol item 5</li>
					<li>ol item 6</li>
					<li>ol item 7</li>
					<li>ol item 8</li>
					<li>ol item 9</li>
					<li>ol item 10</li>
				</ol>
			</div>
		</div>
	</div>
</div>
<!-- // -->
