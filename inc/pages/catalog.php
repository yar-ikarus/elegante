<div class="container content">
	<div class="column-left">
		<?include 'inc/modules/menue-left.php';?>
		<!-- / -->
		<div class="place left-filter">
			<div class="title">
				<span>Критерии поиска</span>
			</div>
			<div class="section">
				<div class="name">Назначение</div>
				<div class="checks">
					<?for ($i=0; $i < 6; $i++) { ?>
					<label>
						<input type="checkbox">
						<span>Назначение</span>
					</label>
					<?}?>
				</div>
			</div>
			<div class="section open">
				<div class="name">Назначение</div>
				<div class="checks">
					<?for ($i=0; $i < 6; $i++) { ?>
					<label>
						<input type="checkbox">
						<span>Назначение</span>
					</label>
					<?}?>
				</div>
			</div>
			<div class="section open">
				<div class="name">Назначение</div>
				<div class="checks">
					<?for ($i=0; $i < 6; $i++) { ?>
					<label>
						<input type="checkbox">
						<span>Назначение</span>
					</label>
					<?}?>
				</div>
				<div class="show-more">
					показать еще
				</div>
			</div>
		</div>
		<!-- / -->		
	</div>
	<div class="column-right page-catalog">
		<div class="breadcrumbs">
			<ul>
				<li><a href="/">Главная</a></li>
				<li><span>Каталог</span></li>
			</ul>
		</div>
		<div class="place show-more-title">
			<div class="title-place"><h1>Унитазы</h1></div>
			<div class="filter">
				<div class="part-0">
					<div class="group">
						<div class="cell size-20 adapt"><span class="btn white">Универсальные</span></div>
						<div class="cell size-20 adapt"><span class="btn white">для кухни</span></div>
						<div class="cell size-20 adapt"><span class="btn white">для гаража</span></div>
						<div class="cell size-20 adapt"><span class="btn white">для самолета</span></div>
						<div class="cell size-20 adapt"><span class="btn white">для музея</span></div>
						<div class="cell size-20 adapt"><span class="btn white">для поезда</span></div>
						<div class="cell size-20 adapt"><span class="btn white">для души</span></div>
						<div class="cell size-20 adapt"><span class="btn white">для забавы</span></div>
						<div class="cell size-20 adapt"><span class="btn white">для ума</span></div>
						<div class="cell size-20 adapt"><span class="btn white">для женщин</span></div>
					</div>
				</div>
				<!-- / -->
				<div class="part-1">
					<div class="group">
						<div class="cell size-33 adapt">
							<div class="filter-title">Ценовой диапозон, руб.</div>
							<div class="filters inputs">
								<div>
									<span>ОТ</span><input type="text" value="0">
								</div>
								<div>
									<span>ДО</span><input type="text" value="999 999">
								</div>
							</div>
						</div>
						<div class="cell size-33 adapt">
							<div class="filter-title">Коэффициент всасывания</div>
							<div class="filters inputs">
								<div>
									<span>ОТ</span><input type="text" value="0">
								</div>
								<div>
									<span>ДО</span><input type="text" value="999 999">
								</div>
							</div>
						</div>
						<div class="cell size-33 adapt">
							<div class="filter-title">Тип унитаза</div>
							<div class="filters checks">
								<label><input type="checkbox"><span>чебурашковый</span></label>
								<label><input type="checkbox"><span>ангидрический</span></label>
								<label><input type="checkbox"><span>квантовый</span></label>
								<label><input type="checkbox"><span>золотой</span></label>
								<label><input type="checkbox"><span>разнообразный</span></label>
								<label><input type="checkbox"><span>загадочный</span></label>
								<label><input type="checkbox"><span>противный</span></label>
							</div>
						</div>
					</div>
				</div>
				<!-- / -->
				<div class="dropdown-list">
					<div class="list">
						<div class="row">
							<ul>
								<?for ($i=0; $i < 10; $i++) {
									$rand = rand(3, 20);
								?>
								<li>
									<span>Родитель <?=$rand?></span>
									<div class="drop">
										<? for ($a=0; $a < $rand; $a++) { ?>
										<label><input type="checkbox"><span>значение</span></label>
										<?}?>
									</div>
								</li>
								<?}?>
							</ul>							
						</div>
						<div class="row">
							<ul>
								<?for ($i=0; $i < 10; $i++) {
									$rand = rand(3, 20);
								?>
								<li>
									<span>Родитель <?=$rand?></span>
									<div class="drop">
										<? for ($a=0; $a < $rand; $a++) { ?>
										<label><input type="checkbox"><span>значение</span></label>
										<?}?>
									</div>
								</li>
								<?}?>
							</ul>							
						</div>
						<div class="row">
							<ul>
								<?for ($i=0; $i < 10; $i++) {
									$rand = rand(3, 20);
								?>
								<li>
									<span>Родитель <?=$rand?></span>
									<div class="drop">
										<? for ($a=0; $a < $rand; $a++) { ?>
										<label><input type="checkbox"><span>значение</span></label>
										<?}?>
									</div>
								</li>
								<?}?>
							</ul>							
						</div>
					</div>	
				</div>
				<!-- / -->
				<div class="product-list-show-more open-dropdown-list">
					<i class="icons-open-dropdown"></i>
					<i class="icons-close-dropdown"></i>
					<span>Открыть расширенный фильтр</span>
				</div>
				<!-- / -->
				<div class="part-2 sort">
					<span>Сортировка:</span>
					<ul>
						<li class="active">По популярности</li>
						<li>Сначала дешевые</li>
						<li>Сначала дорогие</li>
					</ul>
				</div>
			</div>
			<!-- // -->
			<div class="group product-list">
				<? for ($i=0; $i < 16; $i++) { ?>
				<div class="cell">
					<div class="image">
						<div class="img" style="background-image: url(/project/images/other/demo.jpg);">
							<a href="?page=product"></a>
						</div>						
					</div>
					<div class="name"><a href="?page=product">Чугунный унитаз 28 калибра с автоматическим смыванием, коэффициент всасывания 4.9, мощность усилителя 120 ват, питание от солнечной батареи</a></div>
					<div class="descriptions">
						<div>
							<span>Код товара</span>
							<span>181818181</span>
						</div>
						<div>
							<span>Габариты (дшг)</span>
							<span>170х199х99</span>
						</div>
					</div>
					<div class="other-info">
						<p>Скользкий</p>
						<p>Похож на чебурашку</p>
					</div>
					<div class="price">
						<span>Цена</span>
						<span>100 500 р.</span>
					</div>
					<div class="button">
						<span class="btn">В корзину</span>
					</div>
				</div>
				<?}?>				
			</div>
			<!-- // -->
			<div class="paginator">
				<ul>
					<li class="active"><a href="">1</a></li>
					<li><a href="">2</a></li>
					<li><a href="">3</a></li>
					<li><a href="">4</a></li>
					<li><a href="">5</a></li>
					<li><a href="">6</a></li>
					<li><a href="">7</a></li>
					<li><a href="">8</a></li>
					<li><a href="">..</a></li>					
				</ul>				
			</div>
		</div>
	</div>
</div>