<div class="container content">
	<div class="search-page">
		<div class="breadcrumbs">
			<ul>
				<li><a href="/">Главная</a></li>
				<li><span>Поиск</span></li>
			</ul>
		</div>
		<!-- // -->
		<div class="place show-more-title">
			<div class="title-place">
				<h1>Поиск - {запрос}</h1>
			</div>
			<!-- / -->
			<div class="search-filter">
				<div class="group">
					<div class="cell">
						<input type="text" value="{запрос}" placeholder="Запрос">
					</div>
					<div class="cell">
						<select name="">
							<option>Все категории</option>
							<?for ($i=0; $i < 11; $i++) { ?>
							<option>Элемент</option>									
							<?}?>
						</select>
					</div>
					<div class="cell">
						<div>
							<label>
								<input type="checkbox">
								<span>Поиск в подкатегориях</span>
							</label>
							<label>
								<input type="checkbox">
								<span>Искать в описании товаров</span>
							</label>
						</div>
					</div>
					<div class="cell">
						<div class="btn pink">Поиск</div>
					</div>
				</div>
			</div>
			<!-- / -->
			<div class="sort">
				<h2>Результаты поиска</h2>
				<div>
					<span>Сортировка</span>
					<select>
						<option>По умолчанию</option>
						<option>По убыванию</option>
						<option>Еще как нибудь</option>
					</select>
				</div>
				<div>
					<span>Показать</span>
					<select>
						<option>15</option>
						<option>30</option>
						<option>50</option>
						<option>100</option>
						<option>Все</option>
					</select>
				</div>
			</div>
			<!-- / -->
			<div class="product-list results">
				<div class="group">
					<?for ($i=0; $i < 9; $i++) { ?>
					<div class="cell">
						<div class="image">
							<div class="img" style="background-image: url(/project/images/other/demo.jpg);">
								<a href="?page=product"></a>
							</div>						
						</div>
						<div class="name"><a href="?page=product">Чугунный унитаз 28 калибра с автоматическим смыванием, коэффициент всасывания 4.9, мощность усилителя 120 ват, питание от солнечной батареи</a></div>
						<div class="descriptions">
							<div>
								<span>Код товара</span>
								<span>181818181</span>
							</div>
							<div>
								<span>Габариты (дшг)</span>
								<span>170х199х99</span>
							</div>
						</div>
						<div class="other-info">
							<p>Скользкий</p>
							<p>Похож на чебурашку</p>
						</div>
						<div class="price">
							<span>Цена</span>
							<span>100 500 р.</span>
						</div>
						<div class="button">
							<span class="btn">В корзину</span>
						</div>
					</div>	
					<?}?>
				</div>
			</div>
			<!-- / -->
			<div class="paginator">
				<ul>
					<li class="active"><a href="">1</a></li>
					<li><a href="">2</a></li>
					<li><a href="">3</a></li>
					<li><a href="">4</a></li>
					<li><a href="">5</a></li>
					<li><a href="">6</a></li>
					<li><a href="">7</a></li>
					<li><a href="">8</a></li>
					<li><a href="">..</a></li>					
				</ul>				
			</div>
		</div>				
	</div>
</div>