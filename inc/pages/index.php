<div class="container content index-page">
	<?include 'inc/modules/column-left.php';?>
	<div class="column-right index-page">
		<div class="place index-product-list">
			<div class="tabs">
				<div class="active"><i class="icons-tabs-pop"></i><span>Популярные товары</span></div>
				<div><i class="icons-tabs-discount"></i><span>Распродажа</span></div>
			</div>
			<!-- / -->
			<div class="product-category-tags">
				<a href="">Все товары</a>
				<a href="">Мебель для ванной</a>
				<a href="">Смесители</a>
				<a href="">Ванны</a>
				<a href="" class="active">Унитазы</a>
				<a href="">Раковины</a>
				<a href="">Полотенцесушители</a>
			</div>
			<!-- / -->
			<div class="group product-list">
				<? for ($i=0; $i < 8; $i++) { ?>
				<div class="cell">
					<div class="image">
						<div class="img" style="background-image: url(/project/images/other/demo.jpg);">
							<a href="?page=product"></a>
						</div>						
					</div>
					<div class="name"><a href="?page=product">Чугунный унитаз 28 калибра с автоматическим смыванием, коэффициент всасывания 4.9, мощность усилителя 120 ват, питание от солнечной батареи</a></div>
					<div class="descriptions">
						<div>
							<span>Код товара</span>
							<span>181818181</span>
						</div>
						<div>
							<span>Габариты (дшг)</span>
							<span>170х199х99</span>
						</div>
					</div>
					<div class="other-info">
						<p>Скользкий</p>
						<p>Похож на чебурашку</p>
					</div>
					<div class="price">
						<span>Цена</span>
						<span>100 500 р.</span>
					</div>
					<div class="button">
						<span class="btn">В корзину</span>
					</div>
				</div>
				<?}?>
			</div>
			<!-- / -->
			<div class="product-list-show-more">
				<i class="icons-show-more"></i><span>Смотреть еще</span>
			</div>
		</div>
		<!-- / -->
		<div class="place show-more-title index-popular-models-and-seo-text">
			<div class="title-place">
				<h2>как правильно выбрать смеситель</h2>
				<a href="">[подробнее]</a>
			</div>
			<!-- / -->
			<div class="text">
				<p>Смесители являются важной составляющей дизайна ванной комнаты. Современные сифоны выполняют не только сугубо практические задачи, но и выполняют декоративные функции. Разнообразие моделей на современном рынке строительных товаров впечатляет, каждый покупатель сможет выбрать подходящий именно для него вариант. Покупая смесители следует помнить, что модели для ванной и кухни имеют некоторые конструктивные отличия. Действующие механизмы также бывают нескольких вариантов, которые разработаны для наибольшего удобства использования.</p>
			</div>
			<div class="popular-models">
				<h2 class="text-yellow">популярные модели</h2>
				<div class="group product-list">
					<? for ($i=0; $i < 8; $i++) { ?>
					<div class="cell">
						<div class="image">
							<div class="status discount"></div>
							<div class="img" style="background-image: url(/project/images/other/demo.jpg);">
								<a href="?page=product"></a>
							</div>						
						</div>
						<div class="name"><a href="?page=product">Чугунный унитаз 28 калибра с автоматическим смыванием, коэффициент всасывания 4.9, мощность усилителя 120 ват, питание от солнечной батареи</a></div>
						<div class="descriptions">
							<div>
								<span>Код товара</span>
								<span>181818181</span>
							</div>
							<div>
								<span>Габариты (дшг)</span>
								<span>170х199х99</span>
							</div>
						</div>
						<div class="other-info">
							<p>Скользкий</p>
							<p>Похож на чебурашку</p>
						</div>
						<div class="price">
							<span>Цена</span>
							<span>100 500 р.</span>
						</div>
						<div class="button">
							<span class="btn">В корзину</span>
						</div>
					</div>
					<?}?>
				</div>
			</div>
			<div class="product-list-show-more">
				<i class="icons-show-more"></i><span>Смотреть еще</span>
			</div>
		</div>
		<!-- / -->
		<div class="place show-more-title index-promo">
			<div class="title-place">
				<h2>О компании</h2>
				<a href="">[подробнее]</a>
			</div>
			<div class="inner">
				<div class="group about">
					<div class="cell">
						<i class="icons-big-logo"></i>
					</div>
					<div class="cell">
						<p>Elegante - крупнейший в России интернет-магазин, предлагающий сантехнику отечественного и зарубежного производства.<br>
						Мы предлагаем душевые кабины и душевые программы, биде и писсуары, инсталляции, смесители и сифоны, широкий спектр бытового отопительного оборудования, а также мебель и аксессуары для ванной комнаты.</p>
					</div>
				</div>
				<div class="group advantages">
					<div class="cell">
						<i class="icons-index-promo-1"></i>
						<span>лояльные цены и честные акции</span>
					</div>
					<div class="cell">
						<i class="icons-index-promo-2"></i>
						<span>большой выбор продукции</span>
					</div>
					<div class="cell">
						<i class="icons-index-promo-3"></i>
						<span>качественный товар</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>