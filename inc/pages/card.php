<script src="/project/js/lib/zoomsl-3.0.min.js"></script>
<script>
$(function(){
	$(".zoom").imagezoomsl({
		zoomrange: [1, 3],
		cursorshadeborder: "3px solid #C8E6EF",
		magnifierborder: "1px solid #C8E6EF",
		magnifiereffectanimate: "fadeIn",
		magnifierpos: "right"	
	});
});
</script>
<div class="container content">
	<? include 'inc/modules/column-left.php';?>
	<!-- / -->
	<div class="column-right page-card">
		<div class="breadcrumbs">
			<ul>
				<li><a href="/">Главная</a></li>
				<li><a href="/">Унитазы</a></li>
				<li><span>Унитаз Lemark Pramen LM3306C</span></li>
			</ul>
		</div>
		<!-- / -->
		<div class="place product">
			<div class="group">
				<div class="cell image">
					<div class="level-0">
						<img class="zoom" src="/project/images/other/demo.jpg" data-large="/project/images/other/demo.jpg" data-help="Используйте колесико мыши для Zoom +/-">
					</div>
					<div class="level-1">
						<div class="product-slider-small">
							<div class="item"><div class="img" style="background-image:url(/project/images/other/demo-1.jpg)" data-open="/project/images/other/demo-1.jpg"></div></div>
							<div class="item"><div class="img" style="background-image:url(/project/images/other/demo-2.jpg)" data-open="/project/images/other/demo-2.jpg"></div></div>
							<div class="item"><div class="img" style="background-image:url(/project/images/other/demo-3.jpg)" data-open="/project/images/other/demo-3.jpg"></div></div>
							<div class="item"><div class="img" style="background-image:url(/project/images/other/demo-1.jpg)" data-open="/project/images/other/demo-1.jpg"></div></div>
							<div class="item"><div class="img" style="background-image:url(/project/images/other/demo-2.jpg)" data-open="/project/images/other/demo-2.jpg"></div></div>
							<div class="item"><div class="img" style="background-image:url(/project/images/other/demo-3.jpg)" data-open="/project/images/other/demo-3.jpg"></div></div>
							<div class="item"><div class="img" style="background-image:url(/project/images/other/demo-1.jpg)" data-open="/project/images/other/demo-1.jpg"></div></div>
							<div class="item"><div class="img" style="background-image:url(/project/images/other/demo-2.jpg)" data-open="/project/images/other/demo-2.jpg"></div></div>
							<div class="item"><div class="img" style="background-image:url(/project/images/other/demo-3.jpg)" data-open="/project/images/other/demo-3.jpg"></div></div>
						</div>						
					</div>
				</div>
				<div class="cell options">
					<h1>Унитаз Lemark Pramen LM3306C</h1>		
					<p>
						<strong>Код товара:</strong>
						<span>123456</span>
					</p>
					<p>
						<strong>Производство:</strong>
						<span>Lemark</span>
					</p>
					<?for ($i=0; $i < 6; $i++) { ?>
					<p>
						<strong>Еще что нибудь:</strong>
						<span>Данные</span>
					</p>
					<?}?>
					<div class="price-place">
						<div class="price">
							333 333 р.
						</div>
						<div class="buy">
							<div class="btn">КУПИТЬ</div>
						</div>
					</div>
					<div class="more">
						<span>Смотреть всю серию:</span>
						<a href="">Название бренда</a>
					</div>
				</div>
			</div>
		</div>
		<!-- / -->
		<div class="place product-description">
			<div class="tabs">
				<div class="active" data-open="1">Характеристики</div>
				<div data-open="2">комплектация</div>
			</div>
			<div class="hidden-places section-1">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum iste aperiam ducimus dolorem reiciendis accusamus saepe facere! Dolore hic temporibus nobis, rerum tempora doloribus autem dolor ipsam totam quam excepturi esse sequi animi corporis officiis incidunt modi odit nam sunt maxime dicta. Ratione dolorum assumenda earum, fugit facere in corrupti at vitae nobis nesciunt! Pariatur nisi, voluptate consectetur minus enim ea velit a voluptatibus aspernatur, porro quasi voluptatum veniam id quaerat. Ipsam dolorum placeat eveniet iure perspiciatis aperiam, possimus eos tempora aliquid exercitationem modi dolor consequatur velit, sint! Voluptate inventore aperiam pariatur reiciendis molestiae, necessitatibus dolorum. Rerum, facere, accusamus! Distinctio corporis amet, maiores consectetur rem et, perspiciatis dicta.</p>
			</div>
			<div class="hidden-places section-2">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum iste aperiam ducimus dolorem reiciendis accusamus saepe facere! Dolore hic temporibus nobis, rerum tempora doloribus autem dolor ipsam totam quam excepturi esse sequi animi corporis officiis incidunt modi odit nam sunt maxime dicta. Ratione dolorum assumenda earum, fugit facere in corrupti at vitae nobis nesciunt! Pariatur nisi, voluptate consectetur minus enim ea velit a voluptatibus aspernatur, porro quasi voluptatum veniam id quaerat. Ipsam dolorum placeat eveniet iure perspiciatis aperiam, possimus eos tempora aliquid exercitationem modi dolor consequatur velit, sint! Voluptate inventore aperiam pariatur reiciendis molestiae, necessitatibus dolorum. Rerum, facere, accusamus! Distinctio corporis amet, maiores consectetur rem et, perspiciatis dicta.</p>
			</div>
		</div>
		<!-- / -->
		<div class="place show-more-title">
			<div class="title-place"><h2>другие модели серии [Название бренда]</h2></div>
			<div class="group product-list">
				<? for ($i=0; $i < 4; $i++) { ?>
				<div class="cell">
					<div class="image">
						<div class="img" style="background-image: url(/project/images/other/demo.jpg);">
							<a href="?page=product"></a>
						</div>						
					</div>
					<div class="name"><a href="?page=product">Чугунный унитаз 28 калибра с автоматическим смыванием, коэффициент всасывания 4.9, мощность усилителя 120 ват, питание от солнечной батареи</a></div>
					<div class="descriptions">
						<div>
							<span>Код товара</span>
							<span>181818181</span>
						</div>
						<div>
							<span>Габариты (дшг)</span>
							<span>170х199х99</span>
						</div>
					</div>
					<div class="other-info">
						<p>Скользкий</p>
						<p>Похож на чебурашку</p>
					</div>
					<div class="price">
						<span>Цена</span>
						<span>100 500 р.</span>
					</div>
					<div class="button">
						<span class="btn">В корзину</span>
					</div>
				</div>
				<?}?>				
			</div>
		</div>
	</div>
</div>
<!-- // -->
