<div class="container content">
	<? include 'inc/modules/column-left.php';?>
	<!-- / -->
	<div class="column-right page-order">
		<div class="breadcrumbs">
			<ul>
				<li><a href="/">Главная</a></li>
				<li><span>Оформление заказа</span></li>
			</ul>
		</div>
		<!-- / -->
		<div class="place show-more-title order-place">
			<div class="title-place"><h1>Оформление заказа</h1></div>
			<div class="products-table">
				<table>
					<tr>
						<th>Фото</th>
						<th>Название товара</th>
						<th>Цена</th>
						<th>Количество</th>
						<th>Сумма</th>
					</tr>
					<?for ($i=0; $i < 3; $i++) { ?>
					<tr>
						<td><img src="/project/images/other/demo.jpg" alt=""></td>
						<td><a href="">Унитаз Белочка</a></td>
						<td class="unit-price"><span data-price="2500">2 500 руб.</span></td>
						<td>
							<input type="number" value="1">
						</td>
						<td class="group-price">
							<span data-price="2500">2 500 руб.</span>
							<div class="remove"></div>
						</td>
					</tr>					
					<?}?>
					<tr class="final-price">
						<td colspan="5">
							<span>Итого:</span>
							<strong id="final-price" data-price="15 000">15 000 р.</strong>
						</td>
					</tr>
				</table>				
			</div>
			<div class="form">
				<h2>Заполните все поля формы</h2>
				<div class="group">
					<div class="cell size-50 adapt">
						<div>
							<span>Фио</span>
							<input type="text" name="fio" value="">
						</div>
						<div>
							<span>Телефон</span>
							<input type="text" name="phone" class="phone" value="">
						</div>
						<div>
							<span>Электронный адрес</span>
							<input type="text" name="email" value="">
						</div>						
						<div>
							<span>Комментарии к заказу</span>
							<textarea name="comments"></textarea>
						</div>
					</div>
					<div class="cell size-50 adapt">
						<div>
							<span>Адрес доставки</span>
							<input type="text" name="address" value="">
						</div>
						<div>
							<span>Выберите способ оплаты</span>
							<select name="">
								<option value=""></option>
								<option value="">Способ</option>
								<option value="">Способ</option>
								<option value="">Способ</option>
							</select>
						</div>
						<div class="checks">
							<div>
								<label>
									<input type="checkbox">
									<span>Добавить подъем на этаж</span>
								</label>
							</div>
							<div>
								<label>
									<input type="checkbox">
									<span>Добавить услугу установки</span>
								</label>
							</div>
							<div class="space"></div>
							<div>
								<label>
									<input type="checkbox">
									<span>Соглашаюсь с условиями пользовательского соглашения</span>
								</label>
							</div>
							<div class="submit">
								<span class="btn">Оформить заказ</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- / -->
		<div class="place show-more-title response">
			<div class="title-place"><h3>Ваш заказ ПРИНЯТ</h3></div>
			<div class="response-place">
				<div>спасибо, Ваш заказ <span id="order-number">№326556</span> ПРИНЯТ в обработку</div>
				<div>к оплате <span id="to-pay">150 000</span> рублей</div>
				<div>копия заказа отправлена вам на e-mail!</div>
				<div>
					<a href="" class="btn blue">перейти в каталог</a>
				</div>
			</div>
		</div> 
	</div>
</div>
<!-- // -->
